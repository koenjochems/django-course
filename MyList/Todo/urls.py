from django.conf.urls import url
from . import views


urlpatterns             = [
    url(r"^$", views.project_list, name="project_list"),
    url(r"^(?P<project>[-\w]+)/$", views.project_detail, name="project_detail"),
    url(r"^(?P<project_id>\d+)/share/$", views.project_share, name="project_share"),
    url(r"^task/(?P<project>[-\w]+)/$", views.task_list, name="task_list"),
    url(r"^task/(?P<project>[-\w]+)/(?P<task>[-\w]+)/$", views.task_detail, name="task_detail"),
]
