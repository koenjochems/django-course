from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from .models import Project
from .models import Task
from .models import Comment
from .forms import EmailPostForm
from .forms import CommentForm
from .email import PythonGmailAPI

# Create your views here.
def project_list(request):
    projects            = Project.open.all()
    paginator           = Paginator(projects, 3)
    page                = request.GET.get("page")
    
    try:
        projects_page       = paginator.page(page)
    except PageNotAnInteger:
        projects_page       = paginator.page(1)
    except EmptyPage:
        projects_page       = paginator.page(paginator.num_pages())
        
    return render(request, "todo/project/list.html", {"projects": projects_page,
                                                      "page": page})

def project_detail(request, project):
    project             = get_object_or_404(Project, slug=project, status="open")

    # List of active comments for this post
    comments = project.comments.filter(active=True)
    if request.method == 'POST':
        # A comment was posted
        comment_form = CommentForm(data=request.POST)

        if comment_form.is_valid():
            cd                  = comment_form.cleaned_data
            project.comments.create(name=cd["name"], email=cd["email"], body=cd["body"])
    else:
        comment_form = CommentForm()

    return render(request, "todo/project/detail.html", {"project": project,
                                                        "comments": comments,
                                                        "comment_form": comment_form})
    
def task_list(request, project):
    project             = get_object_or_404(Project, slug=project, status="open")
    tasks               = Task.objects.filter(project__title=project.title, status="pending")
    paginator           = Paginator(tasks, 3)
    page                = request.GET.get("page")
    
    try:
        tasks_page       = paginator.page(page)
    except PageNotAnInteger:
        tasks_page       = paginator.page(1)
    except EmptyPage:
        tasks_page       = paginator.page(paginator.num_pages())
        
    return render(request, "todo/task/list.html", {"tasks": tasks_page,
                                                   "page": page})

def task_detail(request, project, task):
    project             = get_object_or_404(Project, slug=project, status="open")
    task                = get_object_or_404(Task, slug=task, project=project)
    
    # List of active comments for this post
    comments = task.comments.filter(active=True)
    if request.method == 'POST':
        # A comment was posted
        comment_form = CommentForm(data=request.POST)

        if comment_form.is_valid():
            cd                  = comment_form.cleaned_data
            task.comments.create(name=cd["name"], email=cd["email"], body=cd["body"])
    else:
        comment_form = CommentForm()
    
    return render(request, "todo/task/detail.html", {"task": task,
                                                     "comments": comments,
                                                     "comment_form": comment_form})

def project_share(request, project_id):
    project             = get_object_or_404(Project, id=project_id, status="open")
    sent                = False
    
    if request.method == "POST":
        form                = EmailPostForm(request.POST)
        if form.is_valid():
            cd                  = form.cleaned_data
            project_url         = request.build_absolute_uri(project.get_absolute_url())
            subject             = "{} ({}) recommends you reading '{}'".format(cd["name"], cd["email"], project.title)
            message             = "Read '{}' at {}\n\n{}'s comments: {}".format(project.title, project_url, cd['name'], cd['comments'])
            PythonGmailAPI().gmail_send("koen.jochems@gmail.com", cd['to'], subject, message)
            sent = True
    else:
        form = EmailPostForm()
        
    return render(request, 'todo/project/share.html', {"project": project,
                                                       "form": form,
                                                       "sent": sent})
    