from django.contrib import admin
from .models import Project
from .models import Task
from .models import Comment

class ProjectAdmin(admin.ModelAdmin):
    list_display        = ("title", "slug", "author", "created", "status")
    prepopulated_fields = {"slug": ("title",)}
    raw_id_fields       = ("author",)

class TaskAdmin(admin.ModelAdmin):
    list_display        = ("project", "title", "slug", "author", "created", "status")
    prepopulated_fields = {"slug": ("title",)}
    raw_id_fields       = ("author",)

class CommentAdmin(admin.ModelAdmin):
    list_display        = ("name", "email", "content_object", "created", "active")
    list_filter         = ("active", "created", "updated")
    search_fields       = ("name", "email", "body")
    autocomplete_lookup_fields = {
        'generic': [['content_type', 'object_id']],
    }
    
# Register your models here.
admin.site.register(Project, ProjectAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(Comment, CommentAdmin)
