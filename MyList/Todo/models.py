from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType


class Comment(models.Model):
    limit               = models.Q(app_label="Todo", model="project") | models.Q(app_label="Todo", model="task")
        
    content_type        = models.ForeignKey(ContentType, on_delete=models.CASCADE, limit_choices_to=limit)
    object_id           = models.PositiveIntegerField()
    content_object      = GenericForeignKey('content_type', 'object_id')
    name                = models.CharField(max_length=80)
    email               = models.EmailField()
    body                = models.TextField()
    created             = models.DateTimeField(auto_now_add=True)
    updated             = models.DateTimeField(auto_now=True)
    active              = models.BooleanField(default=True)

    class Meta:
        ordering = ("created",)

    def __str__(self):
        return "Comment by {} on {}".format(self.name, self.content_object)

# Create your models here.
class OpenManager(models.Manager):
    def get_queryset(self):
        return super(OpenManager, self).get_queryset().filter(status="open")

class PendingManager(models.Manager):
    def get_queryset(self):
        return super(PendingManager, self).get_queryset().filter(status="pending")

class ID(models.Model):
    objects             = models.Manager()
    
    title               = models.CharField(max_length=250)
    slug                = models.SlugField(max_length=250, unique_for_date="created")
    author              = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_author")
    body                = models.TextField()
    created             = models.DateTimeField(auto_now_add=True)
    order               = models.PositiveIntegerField()
    
    comments            = GenericRelation(Comment)
    
    class Meta:
        abstract            = True
        ordering            = ("order",)
    
    def __str__(self):
        return self.title
        
class Project(ID):
    STATUS_CHOICES      = (
        ("open", "Open"),
        ("closed", "Closed")
    )
    
    closed              = models.DateTimeField(auto_now=True)
    status              = models.CharField(max_length=20, choices=STATUS_CHOICES, default="open")
    
    open                = OpenManager()
    
    def get_absolute_url(self):
        return reverse("todo:project_detail", args=[self.slug])
    
    def get_tasks_url(self):
        return reverse("todo:task_list", args=[self.slug])
    
class Task(ID):
    STATUS_CHOICES  = (
        ("pending", "Pending"),
        ("in_progress", "In progress"),
        ("completed", "Completed")
    )
    
    completed           = models.DateTimeField(auto_now=True)
    status              = models.CharField(max_length=20, choices=STATUS_CHOICES, default="pending")
    project             = models.ForeignKey(Project, related_name="todo_tasks")
    
    pending             = PendingManager()
    
    def get_absolute_url(self):
        return reverse("todo:task_detail", args=[self.project.slug, self.slug])
